package com.example.sharing.constants;

public enum ChangePermissionAction {
    ADD,
    MODIFY,
    DELETE
}
