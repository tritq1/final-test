package com.example.sharing.constants;

public enum PermissionType {
    READ,
    SHARE,
    OWNER
}
