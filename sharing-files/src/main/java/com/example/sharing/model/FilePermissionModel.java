package com.example.sharing.model;

import javax.persistence.*;

@Entity
@Table(name = "file_permission")
public class FilePermissionModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "file_permission_id")
    private int filePermissionId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "file_id")
    private FileModel file;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserModel user;

    @Column(name = "read_permission")
    private boolean readPermission;

    @Column(name = "share_permission")
    private boolean sharePermission;

    @Column(name = "owner")
    private boolean owner;

    public int getFilePermissionId() {
        return filePermissionId;
    }

    public void setFilePermissionId(int filePermissionId) {
        this.filePermissionId = filePermissionId;
    }

    public FileModel getFile() {
        return file;
    }

    public void setFile(FileModel file) {
        this.file = file;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public boolean isReadPermission() {
        return readPermission;
    }

    public void setReadPermission(boolean readPermission) {
        this.readPermission = readPermission;
    }

    public boolean isSharePermission() {
        return sharePermission;
    }

    public void setSharePermission(boolean sharePermission) {
        this.sharePermission = sharePermission;
    }

    public boolean isOwner() {
        return owner;
    }

    public void setOwner(boolean owner) {
        this.owner = owner;
    }
}
