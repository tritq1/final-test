package com.example.sharing.config;

public class UserLoginContext {
    public static int userDid;
    public static String userName;

    public static String getUserName() {
        return userName;
    }

    public static void setUserName(String userName) {
        UserLoginContext.userName = userName;
    }

    public static int getUserDid() {
        return userDid;
    }

    public static void setUserDid(int userDid) {
        UserLoginContext.userDid = userDid;
    }
}
