package com.example.sharing.controller;

import com.example.sharing.domain.LoginRequestDto;
import com.example.sharing.domain.LoginResponseDto;
import com.example.sharing.service.impl.UserDetailServiceImpl;
import com.example.sharing.utils.JwtTokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v1")
public class AuthenticationController {
    @Autowired
    private JwtTokenUtils jwtTokenUtils;

    @Autowired
    private UserDetailServiceImpl userDetailsService;

    @Autowired
    protected AuthenticationManager authenticationManager;

    @PostMapping(value = "/login", consumes = MediaType.ALL_VALUE)
    public ResponseEntity authenUser(@RequestBody LoginRequestDto loginRequestDto) {
        authenticate(loginRequestDto.getUsername(), loginRequestDto.getPassword());
        final UserDetails userDetails = userDetailsService.loadUserByUsername(loginRequestDto.getUsername());
        final String token = jwtTokenUtils.generateToken(userDetails);

        return ResponseEntity.ok(new LoginResponseDto(token));
    }

    private void authenticate(String userName, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, password));
    }
}
