package com.example.sharing.controller;

import com.example.sharing.config.UserLoginContext;
import com.example.sharing.domain.ChangePermissionRequestDto;
import com.example.sharing.domain.CommonResponseDto;
import com.example.sharing.domain.SearchingFileRequestDto;
import com.example.sharing.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

import java.io.IOException;
import java.nio.file.AccessDeniedException;

import static com.example.sharing.constants.Constants.FILE_NOT_FOUND;

@RestController
@RequestMapping("/api/v1/files")
public class FileSharingController {
    @Autowired
    private FileService fileService;

    @PostMapping
    public ResponseEntity uploadFile(@RequestParam("file") MultipartFile file) {
        return fileService.storeFile(file);
    }

    @GetMapping(value = "/download")
    public ResponseEntity<Resource> downloadFile(@RequestParam("file-id") int fileId,
                                                 HttpServletRequest request) {
        CommonResponseDto commonResponseDto = new CommonResponseDto();

        try {
            Resource resource = fileService.downloadFile(UserLoginContext.getUserDid(), fileId);
            String contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());

            if (contentType == null) {
                contentType = MediaType.APPLICATION_OCTET_STREAM_VALUE;
            }

            return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);
        } catch (AccessDeniedException ex) {
            commonResponseDto.setMessage(ex.getMessage());
            commonResponseDto.setStatus(false);
            return new ResponseEntity(commonResponseDto, HttpStatus.FORBIDDEN);
        }  catch (IOException e) {
            commonResponseDto.setMessage(FILE_NOT_FOUND + fileId);
            commonResponseDto.setStatus(false);
            return new ResponseEntity(commonResponseDto, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/permission")
    public ResponseEntity<?> changePermission(@RequestBody ChangePermissionRequestDto requestDto) {
        boolean changePermission = fileService.changeFilePermissionOfUser(requestDto.getUserId(),
                requestDto.getFileId(), requestDto.getAction(), requestDto.getPermissionType());
        CommonResponseDto changePermissionResponse = new CommonResponseDto();
        if (changePermission) {
            changePermissionResponse.setStatus(true);
            changePermissionResponse.setMessage("Change Permission Successfully!");
        } else {
            changePermissionResponse.setStatus(false);
            changePermissionResponse.setMessage("Change Permission Failed!");
        }

        return new ResponseEntity<>(changePermissionResponse, HttpStatus.OK);
    }

    @PostMapping("/search")
    public ResponseEntity<?> searchFile(@RequestBody SearchingFileRequestDto searchingFileRequestDto) {
        return new ResponseEntity<>(fileService
                .searchFileByFileNameAndRole(UserLoginContext.getUserDid(),
                        searchingFileRequestDto.getFileName(),
                        searchingFileRequestDto.getPermissionType()), HttpStatus.OK);
    }

    @DeleteMapping("/{file-id}")
    public ResponseEntity<?> deleteFileByOwner(@PathVariable("file-id") int fileId) {
        CommonResponseDto commonResponseDto = new CommonResponseDto();
        if (fileService.deleteFileByOwner(fileId)) {
            commonResponseDto.setStatus(true);
            commonResponseDto.setMessage("File with " + fileId + " has been deleted.");
        } else {
            commonResponseDto.setStatus(false);
            commonResponseDto.setMessage("File Id " + fileId + "delete failed!");
        }
        return new ResponseEntity<>(commonResponseDto, HttpStatus.OK);
    }
}
