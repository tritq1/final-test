package com.example.sharing.controller;

import com.example.sharing.domain.LoginRequestDto;
import com.example.sharing.domain.UserPermissionResponseDto;
import com.example.sharing.model.UserModel;
import com.example.sharing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/users")
    public ResponseEntity<?> createNewUser(@RequestBody LoginRequestDto loginRequestDto) {
        UserModel user = userService.createUser(loginRequestDto);

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping("/users")
    public ResponseEntity<?> getListUserToGrantPermission(@RequestParam("file-id") int fileId) {
        List<UserPermissionResponseDto> responseDtoList = userService.getListUserToGrantPermission(fileId);
        return new ResponseEntity<>(responseDtoList, HttpStatus.OK);
    }
}
