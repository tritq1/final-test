package com.example.sharing.repository;

import com.example.sharing.model.FileModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface FileRepository extends JpaRepository<FileModel, Integer> {
    @Query("SELECT uf FROM FileModel uf WHERE uf.fileName = :fileName and uf.userModel.userId = :userId")
    FileModel getUserFileModelByFileNameAndUserId(String fileName, int userId);

    @Query("SELECT f FROM FileModel f WHERE f.fileId = :fileId")
    FileModel getFileModelByFileId(int fileId);

    @Transactional
    @Modifying
    @Query("DELETE FROM FileModel f WHERE f.fileId = :fileId")
    int deleteByFileId(@Param("fileId") int fileId);
}
