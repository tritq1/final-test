package com.example.sharing.repository;

import com.example.sharing.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends JpaRepository<UserModel, Integer> {
    UserModel getByUsername(String username);

    @Query("SELECT u FROM UserModel u WHERE u.userId NOT IN :userHasPermissions")
    List<UserModel> getListUserWithoutPermission(List<Integer> userHasPermissions);
}
