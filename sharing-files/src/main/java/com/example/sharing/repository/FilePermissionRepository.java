package com.example.sharing.repository;

import com.example.sharing.model.FileModel;
import com.example.sharing.model.FilePermissionModel;
import com.example.sharing.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface FilePermissionRepository extends JpaRepository<FilePermissionModel, Integer> {
    List<FilePermissionModel> getAllByUser(UserModel userModel);

    @Query("SELECT fp FROM FilePermissionModel fp WHERE fp.file.fileId = :fileId AND fp.user.userId = :userId")
    FilePermissionModel getFilePermissionModelByFileAndUser(@Param("userId") int userId, @Param("fileId") int fileId);

    @Transactional
    @Modifying
    @Query("DELETE FROM FilePermissionModel fp WHERE fp.file.fileId = :fileId AND fp.user.userId = :userId")
    int deleteByFileAndUser(@Param("userId") int userId, @Param("fileId") int fileId);

    List<FilePermissionModel> getFilePermissionModelByFile(FileModel fileModel);

    @Query("SELECT fp FROM FilePermissionModel fp " +
            "WHERE (:fileName IS NULL OR LOWER(fp.file.fileName) LIKE LOWER(CONCAT('%',:fileName,'%'))) " +
            "AND fp.owner = :owner AND fp.readPermission = :read " +
            "AND fp.sharePermission = :share AND fp.user.userId = :userId")
    List<FilePermissionModel> searchFileByFileNameAndRole(@Param("fileName") String fileName,
                                                          @Param("owner") boolean owner,
                                                          @Param("read") boolean read,
                                                          @Param("share") boolean share,
                                                          @Param("userId") int userId);

    @Query("SELECT fp FROM FilePermissionModel fp " +
            "WHERE (:fileName IS NULL OR LOWER(fp.file.fileName) LIKE LOWER(CONCAT('%',:fileName,'%'))) " +
            "AND fp.user.userId = :userId")
    List<FilePermissionModel> searchFileByFileName(@Param("fileName") String fileName,
                                                   @Param("userId") int userId);

    @Transactional
    @Modifying
    @Query("DELETE FROM FilePermissionModel fp WHERE fp.file.fileId = :fileId")
    int deleteByFile(@Param("fileId") int fileId);
}
