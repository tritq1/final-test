package com.example.sharing.domain;

import com.example.sharing.constants.PermissionType;

public class SearchingFileRequestDto {
    private String fileName;
    private PermissionType permissionType;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public PermissionType getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(PermissionType permissionType) {
        this.permissionType = permissionType;
    }
}
