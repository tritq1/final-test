package com.example.sharing.domain;

public class LoginResponseDto {
    private String tokenId;

    public LoginResponseDto(String tokenId) {
        this.tokenId = "Bearer " + tokenId;
    }

    public String getTokenId() {
        return tokenId;
    }
}
