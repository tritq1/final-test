package com.example.sharing.domain;

import java.util.List;

public class FileResponseDto {
    List<FileDto> data;

    public List<FileDto> getData() {
        return data;
    }

    public void setData(List<FileDto> data) {
        this.data = data;
    }
}
