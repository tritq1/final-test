package com.example.sharing.domain;

import com.example.sharing.constants.ChangePermissionAction;
import com.example.sharing.constants.PermissionType;

public class ChangePermissionRequestDto {
    private int userId;
    private int fileId;
    private ChangePermissionAction action;
    private PermissionType permissionType;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    public ChangePermissionAction getAction() {
        return action;
    }

    public void setAction(ChangePermissionAction action) {
        this.action = action;
    }

    public PermissionType getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(PermissionType permissionType) {
        this.permissionType = permissionType;
    }
}
