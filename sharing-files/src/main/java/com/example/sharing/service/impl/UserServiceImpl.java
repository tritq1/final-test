package com.example.sharing.service.impl;

import com.example.sharing.domain.LoginRequestDto;
import com.example.sharing.domain.UserPermissionResponseDto;
import com.example.sharing.model.FileModel;
import com.example.sharing.model.FilePermissionModel;
import com.example.sharing.model.UserModel;
import com.example.sharing.repository.FilePermissionRepository;
import com.example.sharing.repository.UserRepository;
import com.example.sharing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FilePermissionRepository filePermissionRepository;

    @Override
    public UserModel createUser(LoginRequestDto requestDto) {
        UserModel user = new UserModel();
        user.setUsername(requestDto.getUsername());

        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        user.setPassword(passwordEncoder.encode(requestDto.getPassword()));

        return userRepository.save(user);
    }

    @Override
    public List<UserPermissionResponseDto> getListUserToGrantPermission(int fileId) {
        FileModel fileModel = new FileModel();
        fileModel.setFileId(fileId);
        List<FilePermissionModel> filePermissionModels = filePermissionRepository.getFilePermissionModelByFile(fileModel);

        List<UserPermissionResponseDto> userPermissionResponseDtos = new ArrayList<>();
        for (FilePermissionModel filePermission: filePermissionModels) {
            UserPermissionResponseDto userPermissionResponseDto = new UserPermissionResponseDto();
            userPermissionResponseDto.setOwner(filePermission.isOwner());
            userPermissionResponseDto.setRead(filePermission.isReadPermission());
            userPermissionResponseDto.setShare(filePermission.isSharePermission());
            userPermissionResponseDto.setUserId(filePermission.getUser().getUserId());
            userPermissionResponseDto.setUserName(filePermission.getUser().getUsername());
            userPermissionResponseDtos.add(userPermissionResponseDto);
        }

        List<Integer> userIdHasPermissions = new ArrayList<>();
        userPermissionResponseDtos.forEach(userPermissionResponseDto -> {
            userIdHasPermissions.add(userPermissionResponseDto.getUserId());
        });

        List<UserModel> listUserNoPermission = userRepository.getListUserWithoutPermission(userIdHasPermissions);
        for (UserModel user: listUserNoPermission) {
            UserPermissionResponseDto userPermissionResponseDto = new UserPermissionResponseDto();
            userPermissionResponseDto.setUserId(user.getUserId());
            userPermissionResponseDto.setUserName(user.getUsername());
            userPermissionResponseDtos.add(userPermissionResponseDto);
        }

        return userPermissionResponseDtos;
    }
}
