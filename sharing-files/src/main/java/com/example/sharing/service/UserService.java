package com.example.sharing.service;

import com.example.sharing.domain.LoginRequestDto;
import com.example.sharing.domain.UserPermissionResponseDto;
import com.example.sharing.model.UserModel;

import java.util.List;

public interface UserService {
    UserModel createUser(LoginRequestDto requestDto);
    List<UserPermissionResponseDto> getListUserToGrantPermission(int fileId);
}
