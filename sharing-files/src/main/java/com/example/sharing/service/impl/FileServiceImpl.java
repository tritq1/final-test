package com.example.sharing.service.impl;

import com.example.sharing.config.UserLoginContext;
import com.example.sharing.constants.ChangePermissionAction;
import com.example.sharing.constants.PermissionType;
import com.example.sharing.domain.CommonResponseDto;
import com.example.sharing.domain.FileDto;
import com.example.sharing.domain.FileResponseDto;
import com.example.sharing.model.FileModel;
import com.example.sharing.model.FilePermissionModel;
import com.example.sharing.model.UserModel;
import com.example.sharing.repository.FilePermissionRepository;
import com.example.sharing.repository.FileRepository;
import com.example.sharing.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.example.sharing.constants.Constants.FILE_NOT_FOUND;
import static com.example.sharing.constants.Constants.USER_HOME;

@Service
public class FileServiceImpl implements FileService {
    @Value("${file.upload-dir}")
    private String uploadDir;

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private FilePermissionRepository filePermissionRepository;

    /**
     * Function store file to directory on server and insert file name into database.
     * @param file
     * @return
     */
    @Override
    public ResponseEntity storeFile(MultipartFile file) {
        String storeDirectory = System.getProperty(USER_HOME) + uploadDir + File.separator + UserLoginContext.getUserName();
        CommonResponseDto commonResponseDto = new CommonResponseDto();
        try{
            Files.createDirectories(Paths.get(storeDirectory));
            Path fileUpload = Paths.get(storeDirectory).resolve(Objects.requireNonNull(file.getOriginalFilename()));
            Files.copy(file.getInputStream(), fileUpload, StandardCopyOption.REPLACE_EXISTING);

            //set response
            commonResponseDto.setStatus(true);
            commonResponseDto.setMessage("Upload successfully with file name: " + Objects.requireNonNull(file.getOriginalFilename()));

            FileModel userFileModel = fileRepository
                    .getUserFileModelByFileNameAndUserId(Objects.requireNonNull(file.getOriginalFilename()), UserLoginContext.getUserDid());
            //file name is existed in DB -> return success
            if (userFileModel != null) {
                return new ResponseEntity(commonResponseDto, HttpStatus.OK);
            }

            //get user details
            UserModel userModel = new UserModel();
            userModel.setUserId(UserLoginContext.getUserDid());

            //store file name to database if file is not existed.
            userFileModel = new FileModel();
            userFileModel.setFileName(Objects.requireNonNull(file.getOriginalFilename()));
            userFileModel.setUser(userModel);
            FileModel storedFile = fileRepository.save(userFileModel);

            //save file permission for owner user.
            FilePermissionModel filePermissionModel = new FilePermissionModel();
            filePermissionModel.setFile(storedFile);
            filePermissionModel.setOwner(true);
            filePermissionModel.setUser(userModel);
            filePermissionRepository.save(filePermissionModel);

            return new ResponseEntity(commonResponseDto, HttpStatus.OK);
        } catch (IOException ex) {
            commonResponseDto.setStatus(false);
            commonResponseDto.setMessage("Failed upload with file name: " + file.getName());

            return new ResponseEntity(commonResponseDto, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Function get file from service with file name and user id, if file not exist, throw exception.
     * @param userId
     * @param fileId
     * @return resource
     * @throws Exception
     */
    @Override
    public Resource downloadFile(int userId, int fileId) throws FileNotFoundException, AccessDeniedException {
        String storeDirectory = System.getProperty(USER_HOME) + uploadDir + File.separator + UserLoginContext.getUserName();

        FilePermissionModel checkUserPermission = filePermissionRepository.getFilePermissionModelByFileAndUser(userId, fileId);

        if (checkUserPermission.isOwner() || checkUserPermission.isReadPermission() || checkUserPermission.isSharePermission()) {
            FileModel userFileModel = fileRepository.getFileModelByFileId(checkUserPermission.getFile().getFileId());

            if (userFileModel == null) {
                throw new FileNotFoundException(FILE_NOT_FOUND + fileId);
            }

            String fileUri = storeDirectory + File.separator + userFileModel.getFileName();

            try {
                Resource resource = new UrlResource(Paths.get(fileUri).toUri());
                if (resource.exists()) {
                    return resource;
                } else {
                    throw new FileNotFoundException(FILE_NOT_FOUND + fileUri);
                }
            } catch (MalformedURLException e) {
                throw new FileNotFoundException(FILE_NOT_FOUND + fileUri);
            }
        } else {
            throw new AccessDeniedException("You do not have permission to download this file!");
        }
    }

    /**
     * function use to add or modify or delete permission of the user for specific file.
     * @param userId
     * @param fileId
     * @param changePermissionAction
     * @param permissionType
     * @return
     */
    @Override
    public boolean changeFilePermissionOfUser(int userId, int fileId,
                                              ChangePermissionAction changePermissionAction,
                                              PermissionType permissionType) {
        boolean checkResponse = false;
        switch (changePermissionAction) {
            case ADD:
                UserModel userModel = new UserModel();
                userModel.setUserId(userId);
                FileModel fileModel = new FileModel();
                fileModel.setFileId(fileId);
                FilePermissionModel filePermissionModel = new FilePermissionModel();
                filePermissionModel.setUser(userModel);
                filePermissionModel.setFile(fileModel);
                checkUserPermission(permissionType, filePermissionModel);
                filePermissionRepository.save(filePermissionModel);
                checkResponse = true;
                break;
            case MODIFY:
                FilePermissionModel modifyFilePermission = filePermissionRepository
                        .getFilePermissionModelByFileAndUser(userId, fileId);
                if (modifyFilePermission != null) {
                    modifyFilePermission.setOwner(false);
                    modifyFilePermission.setSharePermission(false);
                    modifyFilePermission.setReadPermission(false);
                    checkUserPermission(permissionType, modifyFilePermission);
                    filePermissionRepository.save(modifyFilePermission);
                    checkResponse = true;
                }
                break;
            case DELETE:
                filePermissionRepository.deleteByFileAndUser(userId, fileId);
                break;
        }
        return checkResponse;
    }

    /**
     * search file by file name and permission.
     * @param userId
     * @param fileName
     * @param permissionType
     * @return
     */
    @Override
    public FileResponseDto searchFileByFileNameAndRole(int userId, String fileName, PermissionType permissionType) {
        FileResponseDto fileResponseDto = new FileResponseDto();
        if (permissionType == null) {
            List<FilePermissionModel> filePermissionModels = filePermissionRepository.searchFileByFileName(fileName, userId);
            return convertFilePermissionModelToDto(fileResponseDto, filePermissionModels);
        }

        switch (permissionType) {
            case OWNER:
                List<FilePermissionModel> ownerFilePermissionModels = filePermissionRepository
                        .searchFileByFileNameAndRole(fileName, true, false, false, userId);
                return convertFilePermissionModelToDto(fileResponseDto, ownerFilePermissionModels);
            case SHARE:
                List<FilePermissionModel> shareFilePermissionModels = filePermissionRepository
                        .searchFileByFileNameAndRole(fileName, false, false, true, userId);
                return convertFilePermissionModelToDto(fileResponseDto, shareFilePermissionModels);
            case READ:
                List<FilePermissionModel> readFilePermissionModels = filePermissionRepository
                        .searchFileByFileNameAndRole(fileName, false, true, false, userId);
                return convertFilePermissionModelToDto(fileResponseDto, readFilePermissionModels);
        }
        return fileResponseDto;
    }

    /**
     * delete file by owner
     * @param fileId
     * @return
     */
    @Override
    public boolean deleteFileByOwner(int fileId) {
        //delete record has foreign key.
        int checkDeletedFilePermission = filePermissionRepository.deleteByFile(fileId);

        //delete record has primary key
        int checkDeletedFile = 0;
        if (checkDeletedFilePermission == 1) {
            checkDeletedFile = fileRepository.deleteByFileId(fileId);
        }

        if (checkDeletedFile == 1) {
            return true;
        }
        return false;
    }

    /**
     * convert entity to dto.
     * @param fileResponseDto
     * @param filePermissionModels
     * @return
     */
    private FileResponseDto convertFilePermissionModelToDto(FileResponseDto fileResponseDto, List<FilePermissionModel> filePermissionModels) {
        List<FileDto> fileResponseList = new ArrayList<>();
        if (filePermissionModels != null) {
            for (FilePermissionModel model: filePermissionModels) {
                FileDto fileDto = new FileDto();
                fileDto.setShare(model.isSharePermission());
                fileDto.setRead(model.isReadPermission());
                fileDto.setOwner(model.isOwner());
                fileDto.setFileId(model.getFile().getFileId());
                fileDto.setFileName(model.getFile().getFileName());
                fileResponseList.add(fileDto);
            }
            fileResponseDto.setData(fileResponseList);
        }
        return fileResponseDto;
    }

    /**
     * check type to grant permission.
     * @param permissionType
     * @param filePermissionModel
     */
    private void checkUserPermission(PermissionType permissionType, FilePermissionModel filePermissionModel) {
        switch (permissionType) {
            case READ:
                filePermissionModel.setReadPermission(true);
                break;
            case SHARE:
                filePermissionModel.setSharePermission(true);
                break;
            case OWNER:
                filePermissionModel.setOwner(true);
                break;
        }
    }
}
