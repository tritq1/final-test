package com.example.sharing.service;

import com.example.sharing.constants.ChangePermissionAction;
import com.example.sharing.constants.PermissionType;
import com.example.sharing.domain.FileResponseDto;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.nio.file.AccessDeniedException;

public interface FileService {
    ResponseEntity storeFile(MultipartFile file);
    Resource downloadFile(int userId, int fileId) throws FileNotFoundException, AccessDeniedException;
    boolean changeFilePermissionOfUser(int userId, int fileId,
                                       ChangePermissionAction changePermissionAction, PermissionType permissionType);
    FileResponseDto searchFileByFileNameAndRole(int userId, String fileName, PermissionType permissionType);
    boolean deleteFileByOwner(int fileId);
}
