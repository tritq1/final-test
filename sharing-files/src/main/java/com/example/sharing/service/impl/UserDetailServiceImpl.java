package com.example.sharing.service.impl;

import com.example.sharing.config.UserLoginContext;
import com.example.sharing.model.UserModel;
import com.example.sharing.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        UserModel user = userRepository.getByUsername(userName);
        if (user != null) {
            UserLoginContext.setUserDid(user.getUserId());
            UserLoginContext.setUserName(user.getUsername());
            return new org.springframework.security.core.userdetails.User
                    (user.getUsername(), user.getPassword(), new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("User not found");
        }
    }
}
