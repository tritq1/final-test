package com.example.sharing.service.impl;

import com.example.sharing.domain.LoginRequestDto;
import com.example.sharing.domain.UserPermissionResponseDto;
import com.example.sharing.model.FileModel;
import com.example.sharing.model.FilePermissionModel;
import com.example.sharing.model.UserModel;
import com.example.sharing.repository.FilePermissionRepository;
import com.example.sharing.repository.UserRepository;
import org.assertj.core.util.Arrays;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@SpringBootTest
public class UserServiceImplTest {
    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private FilePermissionRepository filePermissionRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateUser() {
        LoginRequestDto loginRequestDto = new LoginRequestDto();
        loginRequestDto.setUsername("tritq1");
        loginRequestDto.setPassword("123456");

        UserModel userModel = new UserModel();
        userModel.setUserId(1);

        when(userRepository.save(any(UserModel.class))).thenReturn(userModel);
        UserModel userCreated = userService.createUser(loginRequestDto);

        Assert.assertEquals(userCreated.getUserId(), userModel.getUserId());
    }

    @Test
    public void testGetListUserToGrantPermission() {
        FilePermissionModel filePermissionModel = new FilePermissionModel();
        filePermissionModel.setFilePermissionId(1);
        UserModel userModel = new UserModel();
        userModel.setUserId(1);
        userModel.setUsername("tritq1");
        filePermissionModel.setUser(userModel);
        filePermissionModel.setOwner(true);
        List<FilePermissionModel> filePermissionModelList = new ArrayList<>();
        filePermissionModelList.add(filePermissionModel);

        when(filePermissionRepository.getFilePermissionModelByFile(any(FileModel.class))).thenReturn(filePermissionModelList);

        UserModel userNoPermission = new UserModel();
        userNoPermission.setUserId(2);
        userNoPermission.setUsername("tritq2");
        List<UserModel> userModels = new ArrayList<>();
        userModels.add(userNoPermission);

        when(userRepository.getListUserWithoutPermission(anyList())).thenReturn(userModels);

        List<UserPermissionResponseDto> responseDtos = userService.getListUserToGrantPermission(1);
        Assert.assertNotNull(responseDtos);
    }
}
