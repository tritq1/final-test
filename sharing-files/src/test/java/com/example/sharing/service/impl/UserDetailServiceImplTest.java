package com.example.sharing.service.impl;

import com.example.sharing.model.UserModel;
import com.example.sharing.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.mockito.Mockito.when;

@SpringBootTest
public class UserDetailServiceImplTest {
    @InjectMocks
    private UserDetailServiceImpl userDetailService;

    @Mock
    private UserRepository userRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testLoadUserByUserName() throws Exception {
        UserModel userModel = new UserModel();
        userModel.setPassword("123");
        userModel.setUsername("tritq1");
        userModel.setUserId(1);

        when(userRepository.getByUsername(userModel.getUsername())).thenReturn(userModel);

        UserDetails userDetailsResponse = userDetailService.loadUserByUsername(userModel.getUsername());

        Assert.assertEquals(userModel.getUsername(), userDetailsResponse.getUsername());
        Assert.assertEquals(userModel.getPassword(), userDetailsResponse.getPassword());
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testLoadUserByUserNameWithUserNameNotExist() throws Exception {
        UserModel userModel = new UserModel();
        userModel.setUsername("tritq1");

        when(userRepository.getByUsername(userModel.getUsername())).thenReturn(null);

        userDetailService.loadUserByUsername(userModel.getUsername());
    }
}
