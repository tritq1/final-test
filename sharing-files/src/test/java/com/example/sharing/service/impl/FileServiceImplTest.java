package com.example.sharing.service.impl;

import com.example.sharing.config.UserLoginContext;
import com.example.sharing.domain.FileResponseDto;
import com.example.sharing.model.FileModel;
import com.example.sharing.model.FilePermissionModel;
import com.example.sharing.repository.FilePermissionRepository;
import com.example.sharing.repository.FileRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest
public class FileServiceImplTest {
    @InjectMocks
    private FileServiceImpl fileService;

    @Mock
    private FilePermissionRepository filePermissionRepository;

    @Mock
    private FileRepository fileRepository;

    private String uploadDir = "/upload";
    private String storeDirectory;
    private String fileName = "test.txt";

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(fileService, "uploadDir", uploadDir);
        UserLoginContext.setUserName("tritq1");
        UserLoginContext.setUserDid(1);
        storeDirectory = System.getProperty("user.home") + uploadDir + File.separator + UserLoginContext.getUserName();

        File file = new File(storeDirectory + File.separator + fileName);

        try {
            Files.createFile(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @After
    public void deleteFile() {
        File file = new File(storeDirectory + File.separator + fileName);
        try {
            Files.delete(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testStoreFileSuccessfully() {
        File file = new File(storeDirectory + File.separator + fileName);
        file.delete();

        MockMultipartFile mockMultipartFile = new MockMultipartFile("user-file", fileName, "text/plain", "test data".getBytes());

        when(fileRepository.getUserFileModelByFileNameAndUserId(anyString(), anyInt())).thenReturn(null);

        FileModel userFileModel = new FileModel();
        userFileModel.setFileName(fileName);

        when(fileRepository.save(userFileModel)).thenReturn(userFileModel);

        ResponseEntity uploadResponse = fileService.storeFile(mockMultipartFile);
        Assert.assertEquals(HttpStatus.OK.value(), uploadResponse.getStatusCode().value());
    }

    @Test
    public void testStoreFileWithFileExistInDatabase() {
        File file = new File(storeDirectory + File.separator + fileName);
        file.delete();

        MockMultipartFile mockMultipartFile = new MockMultipartFile("user-file", fileName, "text/plain", "test data".getBytes());

        FileModel userFileModel = new FileModel();
        userFileModel.setFileName(fileName);

        when(fileRepository.getUserFileModelByFileNameAndUserId(anyString(), anyInt())).thenReturn(userFileModel);

        ResponseEntity uploadResponse = fileService.storeFile(mockMultipartFile);
        Assert.assertEquals(HttpStatus.OK.value(), uploadResponse.getStatusCode().value());
    }

    @Test
    public void testStoreFileWithException() {
        String fileName = null;
        File file = new File(storeDirectory + File.separator + fileName);
        file.delete();

        MockMultipartFile mockMultipartFile = new MockMultipartFile("user-file", fileName, "text/plain", "test data".getBytes());

        ResponseEntity uploadResponse = fileService.storeFile(mockMultipartFile);
        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), uploadResponse.getStatusCode().value());
    }

    @Test
    public void testDownloadFileSuccessfully() throws Exception {
        FileModel userFileModel = new FileModel();
        userFileModel.setFileId(1);
        userFileModel.setFileName(fileName);

        FilePermissionModel filePermissionModel = new FilePermissionModel();
        filePermissionModel.setOwner(true);
        filePermissionModel.setFile(userFileModel);
        filePermissionModel.setFilePermissionId(1);

        when(filePermissionRepository.getFilePermissionModelByFileAndUser(1, 1)).thenReturn(filePermissionModel);

        when(fileRepository.getFileModelByFileId(1)).thenReturn(userFileModel);

        Resource responseResource = fileService.downloadFile(1, 1);

        Assert.assertNotNull(responseResource);
    }

    @Test(expected = FileNotFoundException.class)
    public void testDownloadFileWithFileNameNotExistInDatabase() throws Exception {
        FileModel userFileModel = new FileModel();
        userFileModel.setFileId(1);
        userFileModel.setFileName(fileName);

        FilePermissionModel filePermissionModel = new FilePermissionModel();
        filePermissionModel.setOwner(true);
        filePermissionModel.setFile(userFileModel);
        filePermissionModel.setFilePermissionId(1);

        when(filePermissionRepository.getFilePermissionModelByFileAndUser(1, 1)).thenReturn(filePermissionModel);
        when(fileRepository.getUserFileModelByFileNameAndUserId(anyString(), anyInt())).thenReturn(null);

        fileService.downloadFile(1, 1);
    }

    @Test(expected = FileNotFoundException.class)
    public void testDownloadFileNotExistInServer() throws Exception {
        FileModel userFileModel = new FileModel();
        userFileModel.setFileId(1);
        userFileModel.setFileName("test1111.txt");

        FilePermissionModel filePermissionModel = new FilePermissionModel();
        filePermissionModel.setOwner(true);
        filePermissionModel.setFile(userFileModel);
        filePermissionModel.setFilePermissionId(1);

        when(filePermissionRepository.getFilePermissionModelByFileAndUser(1, 1)).thenReturn(filePermissionModel);

        when(fileRepository.getFileModelByFileId(1)).thenReturn(userFileModel);

        fileService.downloadFile(1, 1);
    }

    @Test(expected = AccessDeniedException.class)
    public void testForbiddenAccess() throws Exception {
        FileModel userFileModel = new FileModel();
        userFileModel.setFileId(1);
        userFileModel.setFileName(fileName);

        FilePermissionModel filePermissionModel = new FilePermissionModel();
        filePermissionModel.setFile(userFileModel);
        filePermissionModel.setFilePermissionId(1);

        when(filePermissionRepository.getFilePermissionModelByFileAndUser(1, 1)).thenReturn(filePermissionModel);

        when(fileRepository.getFileModelByFileId(1)).thenReturn(userFileModel);

        fileService.downloadFile(1, 1);
    }

    @Test
    public void testDeleteFile() {
        when(filePermissionRepository.deleteByFile(1)).thenReturn(1);
        when(fileRepository.deleteByFileId(1)).thenReturn(1);

        boolean checkResponse = fileService.deleteFileByOwner(1);

        Assert.assertTrue(checkResponse);
    }
}
